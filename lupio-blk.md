# LupIO-BLK specification (v2.0)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog
- 10/25/24
    - LupIO v2.0: Invert field for block size and block count in configure register
- 08/09/24
    - LupIO v1.1: Fix MEMA register description
- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO block device (BLK) specification, which aims to
provide a disk-like interface for second-level storage. It enables the transfer
of blocks from the block device to the system's main memory, and vice-versa.

This device has a single output IRQ.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description                      |
|--------------|---------------|-------------|-----|----------------------------------|
| 0x0          | CONF          | 32          | RO  | Block size and block count       |
| 0x4          | NBLK          | 32          | WO  | Number of blocks to transfer     |
| 0x8          | BLKA          | 32          | WO  | Address of block on device       |
| 0xC          | MEMA          | 32          | WO  | Address in main memory           |
| 0x10         | CTRL          | 32          | WO  | Transfer configuration and start |
| 0x14         | STAT          | 32          | RO  | Device and transfer status       |

### CONF register

This read-only register provides information about the block device: the size of
each block (`block_size`), and the number of blocks total (`block_count`). Both
values are powers of two and guaranteed to be less than 2^32.

| Bitfield | Field name | Size (bits) | Description                                    |
|----------|------------|-------------|------------------------------------------------|
| [31:16]  | BSP2       | 16          | Block size represented as `log2(block_size)`   |
| [15:0]   | NBP2       | 16          | Block count represented as `log2(block_count)` |


### NBLK register

This write-only register defines the number of blocks that should be
transferred. Its value should not be greater than `block_count`. The total
amount of bytes transferred is equal to `NBLK * block_size`.

| Bitfield | Field name | Size (bits) | Description                  |
|----------|------------|-------------|------------------------------|
| [31:0]   | NBLK       | 32          | Number of blocks to transfer |

### BLKA register

This write-only register defines the address of the first block involved in the
transfer.  The first block is numbered `0`, and the last block `block_count -
1`. The value of register `BLKA` should not be greater than `block_count - 1`
neither should be the combination `NBLK + BLKA`.

| Bitfield | Field name | Size (bits) | Description                |
|----------|------------|-------------|----------------------------|
| [31:0]   | BLKA       | 32          | Address of block on device |

### MEMA register

This write-only register defines the physical memory address of the buffer
involved in the transfer.

| Bitfield | Field name | Size (bits) | Description                |
|----------|------------|-------------|----------------------------|
| [31:0]   | MEMA       | 32          | Address in physical memory |

### CTRL register

This write-only register defines the type of transfer (read from block device,
or write to block device) and configures whether the block device should raise
an interrupt once the transfer is completed. Writing to this register starts the
transfer.

| Bitfield | Field name | Size (bits) | Description                                     |
|----------|------------|-------------|-------------------------------------------------|
| [31:2]   | *Reserved* | 30          | *Reserved*                                      |
| [1:1]    | TYPE       | 1           | Type of transfer: `1` to write, `0` to read     |
| [0:0]    | IRQE       | 1           | Interrupt enable: `1` to enable, `0` to disable |

### STAT register

This read-only register provides the current status of the block device and of
the last completed transfer. If an interrupt was pending because of a completed
transfer, reading this register acknowledges the interrupt.

| Bitfield | Field name | Size (bits) | Description                                  |
|----------|------------|-------------|----------------------------------------------|
| [31:3]   | *Reserved* | 29          | *Reserved*                                   |
| [2:2]    | ERRR       | 1           | Transfer error: `1` if error, `0` if success |
| [1:1]    | TYPE       | 1           | Type of transfer: `1` if write, `0` if read  |
| [0:0]    | BUSY       | 1           | Device busy: `1` if busy, `0` if idle        |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_BLK_CONF  0x0
#define LUPIO_BLK_NBLK  0x4
#define LUPIO_BLK_BLKA  0x8
#define LUPIO_BLK_MEMA  0xC
#define LUPIO_BLK_CTRL  0x10
#define LUPIO_BLK_STAT  0x14

/* CTRL/STAT register */
#define LUPIO_BLK_IRQE  0x1 // CTRL
#define LUPIO_BLK_BUSY  0x1 // STAT
#define LUPIO_BLK_TYPE  0x2 // CTRL+STAT
#define LUPIO_BLK_ERRR  0x4 // STAT
```

### Get block device configuration

This code is typically used during the device driver's initialization, to
discover the block size and the number of blocks total.

```c
void lupio_blk_configure(void)
{
    /* Read CONF register */
    conf  = *(volatile uint32_t*)(base + LUPIO_BLK_CONF);

    /* Determine configuration */
    block_size  = 1UL << (conf >> 16);
    block_count = 1UL << (conf & 0xFFFF);
}
```

### Initiate read transfer

This code is typically used to initiate a read transfer from the block device to
system's main memory, and configure the block device to raise an interrupt when
the transfer is completed.

```c
int lupio_blk_init_read(uint32_t nblocks, uint32_t lba, uint32_t *addr)
{
    /* Make sure device is not busy already */
    if (*(volatile uint32_t*)(base + LUPIO_BLK_STAT) & LUPIO_BLK_BUSY)
        return -1;

    /* Configure transfer */
    *(volatile uint32_t*)(base + LUPIO_BLK_NBLK) = nblocks;
    *(volatile uint32_t*)(base + LUPIO_BLK_BLKA) = lba;
    *(volatile uint32_t*)(base + LUPIO_BLK_MEMA) = addr;

    /* Start transfer */
    *(volatile uint32_t*)(base + LUPIO_BLK_CTRL) = LUPIO_BLK_IRQE;

    return 0;
}
```

### Process interrupt

This code is typically used to acknowledge and process the interrupt that the
block device sends once a transfer is completed.

```c
int lupio_blk_ack_req(void)
{
    uint32_t status;

    /* Reading STAT register lowers the IRQ signal */
    status = *(volatile uint32_t*)(base + LUPIO_BLK_STAT);

    if (status & LUPIO_BLK_ERRR) {
        if (status & LUPIO_BLK_TYPE)
            printf("Error write request\n);
        else
            printf("Error read request\n);
        return -1;
    }

    return 0;
}
```
