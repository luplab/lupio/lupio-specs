# LupIO-PIC specification (v1.0)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO programming interrupt controller (PIC)
specification, which can manage input IRQs coming from up to 32 sources.

In non-SMP systems, this device has a single output IRQ.

See below for section about SMP systems.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description                        |
|--------------|---------------|-------------|-----|------------------------------------|
| 0x0          | PRIO          | 32          | RO  | Active input IRQ                   |
| 0x4          | MASK          | 32          | RW  | IRQ mask                           |
| 0x8          | PEND          | 32          | RO  | IRQ pending                        |
| 0xC          | ENAB          | 32          | RW  | IRQ enable *(only in SMP systems)* |

### PRIO register

This read-only register returns the number of the highest-priority, unmasked,
and active input IRQ. The highest-priority input IRQ is from source #0, while
the lowest-priority input IRQ is from source #31. If no input IRQ is currently
active, the register returns value 32.

| Bitfield | Field name | Size (bits) | Description      |
|----------|------------|-------------|------------------|
| [31:0]   | PRIO       | 32          | Active input IRQ |

### MASK register

This read-write register allows to mask or unmask each of the 32 sources
individually. If bit #n of register MASK is set, then input IRQ from source #n
is unmasked.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [31:0]   | MASK       | 32          | IRQ mask    |

### PEND register

This read-only register returns the status of all active IRQs, regardless of
their mask status. If bit #n of register PEND is set, then input IRQ from source
 #n is active.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [31:0]   | PEND       | 32          | IRQ pending |

## SMP support

In order to support SMP systems, the interrupt controller's register
map is duplicated so that each processor has its own private instance.

Each instance has an output IRQ, typically attached to the corresponding
processor.

In order to route any of the 32 input IRQs to any of the processors, each
instance's register map is increased with an extra register, `ENAB`, described
below.

The duplicated register maps are to be aligned on a 16-byte boundary. Bits [3:2]
of the memory address determine the register within a certain instance, while
bits [k:4] determine the instance.

### ENAB register

This read-write register determines which input IRQ is routed to the
corresponding instance. If bit #n of this register is set, then input IRQ #n can
be unmasked or masked and will be seen when becoming active. If bit #n of this
register is unset, then input IRQ #n will not be considered at all.

Upon reset, the ENAB register belonging to the first instance is initialized to
0xFFFFFFFF, which means that all input IRQs are routed to processor #0 by
default.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [31:0]   | ENAB       | 32          | IRQ enable  |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_PIC_PRIO  0x0
#define LUPIO_PIC_MASK  0x4
```

### Unmask input IRQ

This code is typically used to unmask the input IRQ for a certain source.

```c
void lupio_pic_unmask(uint32_t irq)
{
    uint32_t mask, irq_mask = 1UL << irq;

    /* Read current mask */
    mask = *(volatile uint32_t*)(base + LUPIO_PIC_MASK);
    /* Unmask input IRQ */
    mask |= irq_mask;
    /* Update mask register */
    *(volatile uint32_t*)(base + LUPIO_PIC_MASK) = mask;
}
```

### Process interrupt

This code is typically used to determine which source has raised an IRQ.

```c
int lupio_pic_process_irq(void)
{
    uint32_t irq;

    /* Retrieve active input IRQ */
    irq = *(volatile uint32_t*)(base + LUPIO_PIC_PRIO);

    if (irq == 32) {
        printf("Spurious IRQ\n");
        return -1;
    }

    /* Call interrupt handler specific to this source */
    handle_irq(irq);

    return 0;
}
```
