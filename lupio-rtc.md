# LupIO-RTC specification (v2.0)

## Copyright and licence information

© 2020-2023 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 05/30/23
    * LupIO-RTC v2.0: Switch most registers to RW and fix DYYR size
- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO real-time clock (RTC) specification, which can
supply the current date and time to the computer system in ISO 8601 format.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 1-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description      |
|--------------|---------------|-------------|-----|------------------|
| 0x0          | SECD          | 8           | RW  | Second           |
| 0x1          | MINT          | 8           | RW  | Minute           |
| 0x2          | HOUR          | 8           | RW  | Hour             |
| 0x3          | DYMO          | 8           | RW  | Day of the month |
| 0x4          | MNTH          | 8           | RW  | Month            |
| 0x5          | YEAR          | 8           | RW  | Year             |
| 0x6          | CENT          | 8           | RW  | Century          |
| 0x7          | DYWK          | 8           | RO  | Day of the week  |
| 0x8          | DYYR          | 16          | RO  | Day of the year  |

### SECD register

This read-only register returns the second between 0 and 60, where 60 is only
used to denote an added leap second.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [7:0]    | SECD       | 8           | Second (0-60)     |

### MINT register

This read-only register returns the minute between 0 and 59.

| Bitfield | Field name | Size (bits) | Description   |
|----------|------------|-------------|---------------|
| [7:0]    | MINT       | 8           | Minute (0-59) |

### HOUR register

This read-only register returns the hour between 0 and 23.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [7:0]    | HOUR       | 8           | Hour (0-23) |

### DYMO register

This read-only register returns the day of the month between 1 and 31.

| Bitfield | Field name | Size (bits) | Description             |
|----------|------------|-------------|-------------------------|
| [7:0]    | DYMO       | 8           | Day of the month (1-31) |

### MNTH register

This read-only register returns the month between 1 and 12.

| Bitfield | Field name | Size (bits) | Description  |
|----------|------------|-------------|--------------|
| [7:0]    | MNTH       | 8           | Month (1-12) |

### YEAR register

This read-only register returns the year between 0 and 99 (within a century).

Coupled with the CENT register, it can represent any years from 0000 to 9999.

| Bitfield | Field name | Size (bits) | Description  |
|----------|------------|-------------|--------------|
| [7:0]    | YEAR       | 8           | Year (00-99) |

### CENT register

This read-only register returns the century between 0 and 99.

| Bitfield | Field name | Size (bits) | Description     |
|----------|------------|-------------|-----------------|
| [7:0]    | CENT       | 8           | Century (00-99) |

### DYWK register

This read-only register returns the day of the week between 1 and 7, beginning
with Monday and ending with Sunday.

| Bitfield | Field name | Size (bits) | Description           |
|----------|------------|-------------|-----------------------|
| [7:0]    | DYWK       | 8           | Day of the week (1-7) |

### DYYR register

This read-only register returns the day of the year between 1 and 366, where 366
is only used during leap years.

| Bitfield | Field name | Size (bits) | Description             |
|----------|------------|-------------|-------------------------|
| [15:0]   | DYYR       | 16          | Day of the year (1-366) |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_RTC_SECD	0x0
#define LUPIO_RTC_MINT	0x1
#define LUPIO_RTC_HOUR	0x2
#define LUPIO_RTC_DYMO	0x3
#define LUPIO_RTC_MNTH	0x4
#define LUPIO_RTC_YEAR	0x5
#define LUPIO_RTC_CENT	0x6
#define LUPIO_RTC_DYWK	0x7
#define LUPIO_RTC_DYYR	0x8
```

### Read current date and time

This code is typically used to read and print the current date and time in ISO
8601 format.

```c
void lupio_rtc_print_date(void)
{
    int sec, min, hour, mday, mon, year;

    /* Read date and time */
    sec  = *(volatile uint8_t*)(base + LUPIO_RTC_SECD);
    min  = *(volatile uint8_t*)(base + LUPIO_RTC_MINT);
    hour = *(volatile uint8_t*)(base + LUPIO_RTC_HOUR);
    mday = *(volatile uint8_t*)(base + LUPIO_RTC_DYMO);
    mon  = *(volatile uint8_t*)(base + LUPIO_RTC_MNTH);
    year = *(volatile uint8_t*)(base + LUPIO_RTC_YEAR)
            + *(volatile uint8_t*)(base + LUPIO_RTC_CENT) * 100;

    /* Print date and time in ISO 8601 format */
    printf("%d-%d-%dT%d:%d:%d\n", year, mon, mday, hour, min, sec);
}
```
