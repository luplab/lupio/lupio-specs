# LupIO-TTY specification (v1.0)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO terminal (TTY) specification, which can transmit
characters (e.g., to a screen) and receive characters (e.g., from a keyboard).

This device has a single output IRQ, set as a logical OR between the transmitter
and the receiver.

This device supports at least 1-byte characters, and can support up to 4-byte
characters in a implementation-dependent way.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description                        |
|--------------|---------------|-------------|-----|------------------------------------|
| 0x0          | WRIT          | 32          | RW  | Character to transmit              |
| 0x4          | READ          | 32          | RO  | Character received                 |
| 0x8          | CTRL          | 32          | WO  | Transmitter/receiver configuration |
| 0xC          | STAT          | 32          | RO  | Transmitter/receiver status        |

### WRIT register

When writing to WRIT, the provided character gets immediately transmitted.

When reading WRIT, it returns the last transmitted character and lowers the
transmitter part of the device's IRQ if it was raised. If the current character
is still being transmitted or if the last transmitted character has already been
read, then the returned value is undefined.

| Bitfield | Field name | Size (bits) | Description           |
|----------|------------|-------------|-----------------------|
| [31:0]   | WRIT       | 32          | Character to transmit |

### READ register

This read-only register returns the last received character, and lowers the
receiver part of the device's IRQ if it was raised. If no character was
received, then the returned value is undefined.

| Bitfield | Field name | Size (bits) | Description        |
|----------|------------|-------------|--------------------|
| [31:0]   | READ       | 32          | Character received |

### CTRL register

This write-only register configures both transmitter and receiver. If the WBIT
bit is set, then an IRQ will be raised once a character has been successfully
transmitted. If the RBIT bit is set, then an IRQ will be raised when a character
is received.

| Bitfield | Field name | Size (bits) | Description                                    |
|----------|------------|-------------|------------------------------------------------|
| [31:2]   | *Reserved* | 30          | *Reserved*                                     |
| [1:1]    | RBIT       | 1           | Receiver IRQ: `1` to enable, `0` to disable    |
| [0:0]    | WBIT       | 1           | Transmitter IRQ: `1` to enable, `0` to disable |

### STAT register

This read-only register returns the device's status. If the WBIT bit is set,
then the device is ready to transmit a new character. If the RBIT bit is set,
then the device has received a new character, which can be read via register
READ.

| Bitfield | Field name | Size (bits) | Description                                               |
|----------|------------|-------------|-----------------------------------------------------------|
| [31:2]   | *Reserved* | 30          | *Reserved*                                                |
| [1:1]    | RBIT       | 1           | Receiver ready: `1` if character available, `0` if empty |
| [0:0]    | WBIT       | 1           | Transmitter ready: `1` if ready to transmit, `0` if busy |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_TTY_WRIT  0x0
#define LUPIO_TTY_READ  0x4
#define LUPIO_TTY_CTRL  0x8
#define LUPIO_TTY_STAT  0xC

/* CTRL/STAT register */
#define LUPIO_TTY_RBIT  0x2
```

### Write characters

This code is typically used with a zero-latency virtual device, to write
characters to the screen.

```c
void lupio_tty_putchar(const char c)
{
    *(volatile uint32_t*)(base + LUPIO_TTY_WRIT) = c;
}
```

### Read characters

#### Configuration

This code is typically used to configure the TTY to raise an IRQ upon receiving
characters.

```c
void lupio_tty_configure(void)
{
    /* Enable receiver IRQ */
    *(volatile uint32_t*)(base + LUPIO_TTY_CTRL) = LUPIO_TTY_RBIT;
}
```

#### Process interrupt

This code is typically used to acknowledge and process the interrupt that the
TTY sends when a new character has been received.

```c
int lupio_tty_ack_read(uint32_t *c)
{
    uint32_t status;

    /* Reading STAT register lowers the IRQ signal */
    status = *(volatile uint32_t*)(base + LUPIO_TTY_STAT);

    if (!(status & LUPIO_TTY_RBIT)) {
        printf("Spurious IRQ\n");
        return -1;
    }

    /* Read new character */
    *c = *(volatile uint32_t*)(base + LUPIO_TTY_READ);

    return 0;
}
```
