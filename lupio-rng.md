# LupIO-RNG specification (v2.0)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 03/06/23
    * LupIO-RNG v2.0: Rework interrupt logic
- 06/02/21
    * LupIO v1.0: First stable version

## Introduction

This document defines the LupIO random number generator (RNG) specification.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description          |
|--------------|---------------|-------------|-----|----------------------|
| 0x0          | RAND          | 32          | RO  | Random number        |
| 0x4          | SEED          | 32          | RW  | Seed                 |
| 0x8          | CTRL          | 32          | WO  | Device configuration |
| 0xC          | STAT          | 32          | RO  | Device status        |

### RAND register

This read-only register returns a random number. If no random number is
available, then the returned value is undefined.

| Bitfield | Field name | Size (bits) | Description           |
|----------|------------|-------------|-----------------------|
| [31:0]   | RAND       | 32          | Random number         |

### SEED register

This read-write register allows the user to configure the seed value for a
sequence of random numbers. Reading it returns its current value.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [31:0]   | SEED       | 32          | Seed        |

### CTRL register

This write-only register launches the generation of a new random number when
written to. If the IRQE bit is set, then an IRQ is raised when a new random
number is available.

| Bitfield | Field name | Size (bits) | Description                                     |
|----------|------------|-------------|-------------------------------------------------|
| [31:1]   | *Reserved* | 31          | *Reserved*                                      |
| [0:0]    | IRQE       | 1           | Interrupt enable: `1` to enable, `0` to disable |

### STAT register

This read-only register returns the device's status. If the BUSY bit is set,
then no random number is currently available. If the BUSY bit is unset, then
reading this register lowers the IRQ if it was raised.

| Bitfield | Field name | Size (bits) | Description                           |
|----------|------------|-------------|---------------------------------------|
| [31:1]   | *Reserved* | 31          | *Reserved*                            |
| [0:0]    | BUSY       | 1           | Device busy: `1` if busy, `0` if idle |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_RNG_RAND  0x0
#define LUPIO_RNG_CTRL  0x8
#define LUPIO_RNG_STAT  0xC
```

### Read random number

This code is typically used to get a new random number, using polling.

```c
void lupio_rng_read(uint32_t *r)
{
    /* Start random number generation (IRQ disabled) */
    *(volatile uint32_t*)(base + LUPIO_TTY_CTRL) = 0;

    /* Wait until number is available */
    while (*(volatile uint32_t*)(base + LUPIO_TTY_STAT));

    /* Read new random number */
    r = *(volatile uint32_t*)(base + LUPIO_TTY_RAND);
}
```

