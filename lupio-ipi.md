# LupIO-IPI specification (v2.1)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 12/03/24
    - LupIO-IPI v2.1: Fix PEND register R/W
- 07/18/22
    - LupIO-IPI v2.0: Allow for 32 individual IPIs instead of one single IPI
      value
- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO inter-processor interrupt controller (IPI)
specification, which provides to the running software the ability to forcefully
interrupt any processor in an SMP system.

The device's register is duplicated into as many private instances as there are
processors. The device provides as many IRQ outputs as the number of instances,
each typically attached to one processor in the system.

This device supports 32 individual IPIs per instance.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description |
|--------------|---------------|-------------|-----|-------------|
| 0x0          | MASK          | 32          | RW  | IPI mask    |
| 0x4          | PEND          | 32          | RW  | IPI pending |

### MASK register

This read-write register allows to mask or unmask each of the 32 IPIs
individually. If bit #n of register MASK is set, then IPI #n is unmasked.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [31:0]   | MASK       | 32          | IPI mask    |

### PEND register

Each individual bit of the PEND register represents a certain IPI.

If bit #n of both registers PEND and MASK are set, then an output IRQ is raised
for the processor core corresponding to the current instance.

Upon reading, the register returns the status of all IPIs, regardless of
their mask status.

| Bitfield | Field name | Size (bits) | Description |
|----------|------------|-------------|-------------|
| [31:0]   | PEND       | 32          | IPI pending |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_IPI_MASK  0x0
#define LUPIO_IPI_PEND  0x4

#define LUPIO_IPI_INST(idx, reg)	((idx) * 8 + reg)
```

### Unmask IPI

This code is typically used to unmask an IPI for a certain instance.

```c
void lupio_ipi_unmask(int cpuid, uint32_t ipinr)
{
    uint32_t ipi_mask;

    /* Read current mask */
    ipi_mask = *(volatile uint32_t*)(base + LUPIO_IPI_INST(cpuid, LUPIO_IPI_MASK));
    /* Unmask IPI */
    ipi_mask |= 1UL << ipinr;
    /* Update mask register */
    *(volatile uint32_t*)(base + LUPIO_IPI_INST(cpuid, LUPIO_IPI_MASK)) = ipi_mask;
}
```

### Raise IPI

This code is typically used to send an IPI to another processor in the system.

```c
void lupio_ipi_send(int cpuid, uint32_t ipinr)
{
    uint32_t ipi_pend;

    /* Read current pending value */
    ipi_pend = *(volatile uint32_t*)(base + LUPIO_IPI_INST(cpuid, LUPIO_IPI_PEND));
    /* Set IPI */
    ipi_pend |= 1UL << ipinr;
    /* Update pending register */
    *(volatile uint32_t*)(base + LUPIO_IPI_INST(cpuid, LUPIO_IPI_PEND)) = ipi_pend;
}
```

### Process interrupt

This code is typically used to acknowledge and process IPIs.

```c
void lupio_ipi_ack_irq(uint32_t *ipi_val)
{
    int mycpuid = get_my_cpuid();

    /* Read all pending IPIs */
    *ipi_val = *(volatile uint32_t*)(base + LUPIO_IPI_INST(mycpuid, LUPIO_IPI_PEND));
    /* Reset all IPIs to lower IRQ */
    *(volatile uint32_t*)(base + LUPIO_IPI_INST(mycpuid, LUPIO_IPI_PEND)) = 0;
}
```
