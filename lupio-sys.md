# LupIO-SYS specification (v1.1)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 11/15/21
    - LupIO-SYS v1.1: Clarify behavior upon reading registers
- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO system controller (SYS) specification, which
provides a way for the software to halt or reboot the computer system.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description     |
|--------------|---------------|-------------|-----|-----------------|
| 0x0          | HALT          | 32          | RW  | Halt computer   |
| 0x4          | REBT          | 32          | RW  | Reboot computer |

### HALT register

Writing any value into this register halts the computer system, while reading
from it always returns 0.

The interpretation of the value is implementation-dependent. For example, if the
computer system is emulated in software, then writing a value `val` to register
HALT could make the emulator call `exit(val)`. This behavior can be very
convenient for automatic testing.

| Bitfield | Field name | Size (bits) | Description   |
|----------|------------|-------------|---------------|
| [31:0]   | HALT       | 32          | Halt computer |

### REBT register

Writing any value into this register reboots the computer system, while reading
from it always returns 0.

The interpretation of the value is implementation-dependent.

| Bitfield | Field name | Size (bits) | Description     |
|----------|------------|-------------|-----------------|
| [31:0]   | REBT       | 32          | Reboot computer |

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_SYS_HALT  0x0
```

### Halt computer

This code is typically used to halt the computer system.

```c
void lupio_sys_halt(uint32_t code)
{
    /* Power off computer */
    *(volatile uint32_t*)(base + LUPIO_SYS_HALT) = code;

    /* Wait for 100 milliseconds */
    mdelay(100);

    /* Failure */
    printf("Error unable to halt system\n");
    while(1);
}
```

