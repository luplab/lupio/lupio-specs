# LupIO-TMR specification (v1.0)

## Copyright and licence information

© 2020-2022 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

This specification is licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog

- 06/02/21
    - LupIO v1.0: First stable version

## Introduction

This document defines the LupIO timer (TMR) specification, which
provides a real-time counter, and a configurable timer offering periodic and
one-shot modes.

In non-SMP systems, this device has a single output IRQ.

See below for section about SMP systems.

## Register map

The base address of the register map is platform-specific. It should be aligned
on a 4-byte boundary.

| Offset (hex) | Register name | Size (bits) | R/W | Description          |
|--------------|---------------|-------------|-----|----------------------|
| 0x0          | TIME          | 32          | RO  | Real-time counter    |
| 0x4          | LOAD          | 32          | RW  | Reload value         |
| 0x8          | CTRL          | 32          | WO  | Device configuration |
| 0xC          | STAT          | 32          | RO  | Device status        |

### TIME register

This read-only register provides a monotonic real-time counter. Its rate is
implementation-dependent, and the value wraps around when it overflows.

| Bitfield | Field name | Size (bits) | Description                 |
|----------|------------|-------------|-----------------------------|
| [31:0]   | TIME       | 32          | Monotonic real-time counter |

### LOAD register

This read-write register is the reload value for the configurable timer. In
one-shot mode, the timer is initialized with the reload value and then gets
monotonically decremented until it expires when reaching zero. In periodic mode,
the timer is automatically reloaded with the reload value upon expiration.

| Bitfield | Field name | Size (bits) | Description  |
|----------|------------|-------------|--------------|
| [31:0]   | LOAD       | 32          | Reload value |

### CTRL register

This write-only register launches the timer when written to, unless the LOAD
register's value is zero, in which case any currently running timer is
immediately stopped.

If the IRQE bit is set, then the timer will raise an IRQ whenever it expires. If
the PRDC bit is set, then the timer is configured to be periodic instead of
one-shot.

| Bitfield | Field name | Size (bits) | Description                                        |
|----------|------------|-------------|----------------------------------------------------|
| [31:2]   | *Reserved* | 30          | *Reserved*                                         |
| [1:1]    | PRDC       | 1           | Periodic: `1` for periodic timer, `0` for one-shot |
| [0:0]    | IRQE       | 1           | Interrupt enable: `1` to enable, `0` to disable    |

### STAT register

This read-only register returns the device's status. If the EXPD bit is set, it
means that a timer has expired and has not yet been acknowledged. Reading this
register acknowledges the timer expiration and lowers the IRQ if it was raised.

| Bitfield | Field name | Size (bits) | Description                             |
|----------|------------|-------------|-----------------------------------------|
| [31:1]   | *Reserved* | 31          | *Reserved*                              |
| [0:0]    | EXPD       | 1           | Expiration: `1` if expired, `0` if idle |

## SMP support

In order to support SMP systems, the timer's register map can be simply
duplicated so that each processor has its own private instance.

Each instance has an output IRQ, typically attached to the corresponding
processor.

The duplicated register maps are to be aligned on a 16-byte boundary. Bits [3:2]
of the memory address determine the register within a certain instance, while
bits [k:4] determine the instance.

## Software driver example

For all the code examples below, we assume that the device is mapped in memory
at address `base`. We also assume the following definitions:

```c
/* Register map */
#define LUPIO_TMR_LOAD	0x4
#define LUPIO_TMR_CTRL	0x8
#define LUPIO_TMR_STAT	0xC

/* CTRL fields */
#define LUPIO_TMR_IRQE	0x1
#define LUPIO_TMR_PRDC	0x2

/* STAT fields */
#define LUPIO_TMR_EXPD	0x1
```

### Configure periodic timer

This code is typically used to configure a periodic timer.

```c
void lupio_tmr_configure(uint32_t period)
{
    /* Configure period */
    *(volatile uint32_t*)(base + LUPIO_TMR_LOAD) = period;

    /* Start periodic timer */
    *(volatile uint32_t*)(base + LUPIO_TMR_CTRL) = LUPIO_TMR_IRQE |
                                                    LUPIO_TMR_PRDC;
}
```

### Process interrupt

This code is typically used to acknowledge an expired timer after receiving an
IRQ.

```c
int lupio_tmr_ack_irq(void)
{
    uint32_t status;

    /* Reading STAT register lowers the IRQ signal */
    status = *(volatile uint32_t*)(base + LUPIO_TMR_STAT);

    if (!(status & LUPIO_TMR_EXPD)) {
        printf("Spurious IRQ\n");
        return -1;
    }

    return 0;
}
```
