# LupIO specifications

## Copyright and licence information

© 2020 Joël Porquet-Lupine
<[jporquet@ucdavis.edu](mailto:jporquet@ucdavis.edu)>

These specifications are licensed under a [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).

## Changelog
- 12/03/24
    - LupIO-IPI v2.1: Fix PEND register R/W
- 10/25/24
    - LupIO-BLK v2.0: Invert field for block size and block count in configure register
- 08/09/24
    - LupIO-BLK v1.1: Fix MEMA register description
- 05/30/23
    * LupIO-RTC v2.0: Switch most registers to RW and fix DYYR size
- 03/06/23
    * Introduce version numbers per device (major number for API breaking
      changes, minor number for non-breaking changes such as documentation
      improvements)
    * Retroactively apply version numbers to each device
    * LupIO-RNG v2.0: Rework interrupt logic
- 07/18/22
    * LupIO-IPI v2.0: Allow for 32 individual IPIs instead of one single IPI
      value
    * Minor fixes across specifications
- 11/15/21
    * LupIO-SYS v1.1: Clarify behavior upon reading registers
- 06/02/21
    * LupIO v1.0: First stable version

## Introduction

LupIO is a collection of open-source **educational** I/O devices.

This collection defines the interfaces of the most common devices found in
modern RISC-based computers, and makes it possible to build complete systems
using only LupIO devices, even complex multiprocessor (SMP) systems. The
collection includes devices such as an interrupt controller, a timer, an
interprocessor interrupt controller, a terminal, a generic block device, etc.

LupIO devices are intended to be processor-agnostic, so they should be usable
with any processor architecture supporting memory-mapped devices (e.g., RISC-V,
ARM, MIPS, etc.).

Each device interface is designed to be simple and clear, with an optimal
balance between features and complexity. The register maps exposed by the
devices are neatly organized by type (e.g., data, control, and status) and
arranged consistently across devices, in order to ease their programmability.

Developing virtual devices based on these specifications is straightforward (see
proof of concept in [QEMU](https://gitlab.com/luplab/lupio/qemu)), while
developing hardware implementations should be possible as well.

Implementing OS drivers for LupIO devices is also a straightforward process (see
proof of concept in [Linux](https://gitlab.com/luplab/lupio/linux)).

For anyone who wants to learn more about computer organization and operating
systems, LupIO is a great starting place, as it shows realistic yet simple I/O
devices!

## Devices

LupIO defines the following collection of devices (in alphabetical order):

- [LupIO-blk](lupio-blk.md): block device
- [LupIO-ipi](lupio-ipi.md): inter-processor interrupt controller
- [LupIO-pic](lupio-pic.md): programmable interrupt controller
- [LupIO-rng](lupio-rng.md): random number generator
- [LupIO-rtc](lupio-rtc.md): real-time clock
- [LupIO-sys](lupio-sys.md): system controller
- [LupIO-tmr](lupio-tmr.md): timer device
- [LupIO-tty](lupio-tty.md): terminal device

## Background

Real I/O devices are often too complicated for educational purposes. Consider
the venerable line of 8250/16550-compatible UARTs, probably some of the most
ubiquitous devices found across computer systems. It contains 12 internal
registers but only 8 available memory addresses, so students would need to learn
that some registers are multiplexed behind the same memory addresses using a
switch bit. UARTs also provide many complex features such as the ability to
configure the communication parameters (e.g., parity, speed, number of data bits
being transferred, error control, etc.), FIFO buffers, DMA capabilities, etc.
Although it could be possible to help beginner students use a UART by providing
them with a detailed programming sequence, it is unlikely that they would truly
understand how the device works and how to properly use it.

Fictitious devices are the way to go, but no existing project has provided
specifications for a comprehensive collection of educational I/O devices able to
equip a modern SMP system.

For example, the popular
[VirtIO](https://docs.oasis-open.org/virtio/virtio/v1.1/virtio-v1.1.html)
defines a solid list of virtual devices, which includes a network device, a
block device, a console device, etc. However, these devices are all strictly I/O
oriented and do not include other devices necessary to computer systems, such as
an interrupt controller, a timer, etc. Besides, the project's goal is focused
entirely on simulation speed and has no educational aspirations. The
implementations of these virtual devices or corresponding OS drivers definitely
rivals real-life devices in terms of complexity, if not more.

Educational simulators such as [SPIM](http://spimsimulator.sourceforge.net/) or
[MARS](http://courses.missouristate.edu/kenvollmar/mars/) only define one or two
memory-mapped devices. More advanced simulators, such as
[System/161](http://os161.eecs.harvard.edu/documentation/sys161/system.html),
sometimes provide a more complete collection of virtual devices. However, in the
case of System/161, the devices were only designed for this simulator and it
doesn't appear that they would not work seamlessly with other processor
architectures.

[Goldfish](https://android.googlesource.com/kernel/goldfish/) is probably the
most complete collection of simple virtual devices, initially developed by
Google for their QEMU-based Android emulator, and includes many typical devices
(interrupt controller, terminal, timer, etc.). Although these devices were meant
to provide simpler interfaces than realistic ones, the goal was not educational
per se and it sometimes shows. For example, the interfaces are not always
consistent, it's not certain one can build an SMP system only using Goldfish
devices, and there is no generic block device (only a NAND flash device and an
MMC device).
